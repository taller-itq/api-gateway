package com.dlse.taller.gateway.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class JwtRequest {
	
	String svcName;
	String token;

}