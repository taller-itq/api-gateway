package com.dlse.taller.gateway.model;

import lombok.Data;

@Data
public class JwtResponse {
	
	private String claim;
	private Boolean isValid;
	
}
