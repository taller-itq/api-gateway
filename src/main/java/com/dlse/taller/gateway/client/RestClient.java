package com.dlse.taller.gateway.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.http.HttpStatusCode;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.util.UriComponentsBuilder;

import com.dlse.taller.gateway.model.JwtRequest;
import com.dlse.taller.gateway.model.JwtResponse;
import com.dlse.taller.gateway.util.Utils;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@Service
@Slf4j
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class RestClient {

	private final WebClient webClient = WebClient.create();
	
	private final Utils utils;

	public Mono<JwtResponse> doPost(ServerWebExchange exchange, GatewayFilterChain chain, String uri,
			JwtRequest requestBody) {
		log.info("{}",uri);
		log.info("{}",requestBody);
		return webClient
				.post()
				.uri(buildUri())
				.body(Mono.just(requestBody), JwtRequest.class)
				.retrieve()
				.onStatus(HttpStatusCode::is4xxClientError, res -> {
					exchange.getResponse().setStatusCode(res.statusCode());
					return Mono.error(new ResponseStatusException(res.statusCode(), "Client Crror"));
				}).onStatus(HttpStatusCode::is5xxServerError, res -> {
					log.error("error {}", res);
					exchange.getResponse().setStatusCode(res.statusCode());
					return Mono.error(new ResponseStatusException(res.statusCode(), "Server Error"));
				})
				.bodyToMono(JwtResponse.class);
	}

	public String buildUri() {
		String fullPath = utils.jwtPath.concat(utils.jwtUri);
		return UriComponentsBuilder.fromHttpUrl(utils.jwtDomain).port(utils.jwtPort).path(fullPath).toUriString();
	}
	
	public JwtRequest buildRequestBody(String token) {
		return JwtRequest.builder().svcName(utils.SERVICE_NAME).token(token).build();
	}

}
