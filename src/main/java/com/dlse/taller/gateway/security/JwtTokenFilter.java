package com.dlse.taller.gateway.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.server.ResponseStatusException;

import com.dlse.taller.gateway.service.ClaimService;
import com.dlse.taller.gateway.util.Utils;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@Component
@Slf4j
public class JwtTokenFilter extends AbstractGatewayFilterFactory<JwtTokenFilter.Config> {

	private final WebClient.Builder webClientBuilder;

	private ClaimService claimService;
	
	private Utils utils;

	@Autowired
	public JwtTokenFilter(WebClient.Builder webClientBuilder, ClaimService claimService, Utils utils) {
		super(Config.class);
		this.webClientBuilder = webClientBuilder;
		this.claimService = claimService;
		this.utils = utils;
	}

	@Override
	public GatewayFilter apply(Config config) {
		return (exchange, chain) -> {
			String claim = utils.getJwtFromCookies(exchange);
			log.info("claim: {}", claim);
			return claimService.validate(exchange, chain, claim).then(
				Mono.defer(() -> {
					if (!exchange.getResponse().isCommitted() &&
                            HttpStatus.FORBIDDEN.equals(exchange.getResponse().getStatusCode())) {
						return Mono.error(new ResponseStatusException(HttpStatus.FORBIDDEN));
					}
					return chain.filter(exchange);
				})
			);
		};
	}

	public static class Config {
	}
	
}
