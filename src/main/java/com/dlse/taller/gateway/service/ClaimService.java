package com.dlse.taller.gateway.service;

import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.web.server.ServerWebExchange;

import com.dlse.taller.gateway.model.JwtResponse;

import reactor.core.publisher.Mono;

public interface ClaimService {
	
	public Mono<JwtResponse> validate(ServerWebExchange exchange, 
			GatewayFilterChain chain,
			String token);

}
