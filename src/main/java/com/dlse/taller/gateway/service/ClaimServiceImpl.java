package com.dlse.taller.gateway.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ServerWebExchange;

import com.dlse.taller.gateway.client.RestClient;
import com.dlse.taller.gateway.model.JwtRequest;
import com.dlse.taller.gateway.model.JwtResponse;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

@Service
@Slf4j
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class ClaimServiceImpl implements ClaimService{
	
	private final RestClient restClient;

	@Override
	public Mono<JwtResponse> validate(ServerWebExchange exchange, 
			GatewayFilterChain chain,
			String token) {
		log.info("token: {}", token);
		try {
			String uri = restClient.buildUri();
			log.info("uri: {}", uri);
			JwtRequest body = restClient.buildRequestBody(token);
			log.info("body: {}", body);
			return restClient.doPost(exchange, chain, uri, body);
		} catch(Exception e) {
			log.error("error", e);
		} finally {
		}
		return null;	
	}

}
