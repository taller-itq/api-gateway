package com.dlse.taller.gateway.util;

import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;

@Component
public class Utils {
	
	@Value("${services.jwt.uri}")
	public String  jwtUri;
	
	@Value("${services.jwt.domain}")
	public String  jwtDomain;
	
	@Value("${services.jwt.port}")
	public String  jwtPort;
	
	@Value("${services.jwt.path}")
	public String  jwtPath;
	
	public static final String HEADER_NAME = "claim";
	
	public static final String SERVICE_NAME = "api-gateway";

	public String getJwtFromCookies(ServerWebExchange exchange) {
		
		if(ObjectUtils.isNotEmpty(exchange)) {
			return exchange.getRequest()
					.getHeaders()
					.get(HEADER_NAME)
					.toString()
					.replace("[", "").replace("]", "");
		}
		return "";
	}

}